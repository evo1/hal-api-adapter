import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | issues/detail', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:issues/detail');
    assert.ok(route);
  });
});
