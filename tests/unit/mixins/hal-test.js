import EmberObject from '@ember/object';
import HalMixin from 'hal-api-work/mixins/hal';
import { module, test } from 'qunit';

module('Unit | Mixin | hal', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let HalObject = EmberObject.extend(HalMixin);
    let subject = HalObject.create();
    assert.ok(subject);
  });
});
