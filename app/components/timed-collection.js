import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { set } from '@ember/object';

export default Component.extend({
  store: service(),

  init() {
    this._super(...arguments);
    this.timings = [];
  },

  didReceiveAttrs() {
    let shouldRun = this.run;

    if (shouldRun) {
      this._start();
    } else if (shouldRun === false) {
      this._end();
    }
  },

  _start() {
    let collection = this.getCollection();
    set(this, 'collection', collection);
  },

  _end() {
  },

  async getCollection() {
    let t0 = performance.now();
    let collection = null;
    if (this.type === 'findAll') {
      collection = await this.store.findAll('issue', {reload: true});
    } else {
      collection = await this.store.query('issue', { embed: 1 });
    }
    let t1 = performance.now();
    this.timings.pushObject(`fully resolved collection for ${this.type} took ${(t1 - t0)} milliseconds.`);
    set(this, 'run', false);
    return collection;
  },

  actions: {
    doItNow() {
      this._start();
    }
  }
});
