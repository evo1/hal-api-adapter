import DS from 'ember-data';
import { all }  from 'rsvp';

export default DS.JSONAPIAdapter.extend({
  host: '/api',

  init() {
    this._super(...arguments);
    this.headers = {
      'Content-Type': 'application/vnd.api+json'
    }
  },

  async loadCollection(emptyModel) {
    let model = await emptyModel;

    let result = [];

    model.data.forEach((item) => {
      result.push(this.ajax(item.links.self, 'GET'))
    });

    let resolutions = await all(result);

    resolutions.forEach((obj, index) => {
      model.data[index].attributes = obj.data.attributes;
      model.data[index].relationships = obj.data.relationships;
    });

    return model;
  },

  findHasMany() {
    return this.loadCollection(this._super(...arguments));
  },

  findAll(){
    return this.loadCollection(this._super(...arguments));
  },

  findMany(){
    return this.loadCollection(this._super(...arguments));
  },

  query(){
    return this.loadCollection(this._super(...arguments));
  }
});
