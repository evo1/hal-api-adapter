import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('property', function() {
    this.route('detail');
  });
  this.route('issues', function() {
    this.route('detail');
  });
  this.route('landlord');
});

export default Router;
