import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  property: DS.belongsTo('property', {inverse: 'issues'}),
});
