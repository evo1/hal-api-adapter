import DS from 'ember-data';

export default DS.Model.extend({
  address: DS.belongsTo('property-address'),
  issues: DS.hasMany('issue', {inverse: 'property'}),
  landlord: DS.belongsTo('landlord', {inverse: 'properties'})
});
