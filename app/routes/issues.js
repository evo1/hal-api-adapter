import Route from '@ember/routing/route';
import { all }  from 'rsvp';
import { set } from '@ember/object';
import { typeOf }  from '@ember/utils';

let resolveRelations = async function(model, [...relations]) {
  console.log(relations);

  let relationGraph = {};

  relations.forEach((arg) => {
    // if nested relationships
    if (arg.includes('.')) {
      let relationships = arg.split('.');

      relationships.forEach((relationship, index) => {
        relationGraph[relationship] = {promises: []};

        if (index !== 0) {
          relationGraph[relationships[index - 1]][relationship] = {promises: []};
        }
      })
    }
  });

  console.log(relationGraph);

  // let noDependantRelations = [];
  // model.forEach((resource) => {
  //   Object.keys(relationGraph).forEach((key) => {
  //     if (typeOf(relationGraph[key]) === 'array') {
  //       noDependantRelations.push(resource.get(key));
  //     }
  //   });
  // });
  // await all(noDependantRelations);



  return true;
}

export default Route.extend({

  async model() {
    let t0 = performance.now();
    let model = await this.store.findAll('issue');

    // let properties = [];
    // let addresses = [];

    // make this work
    await resolveRelations(model, ['property.property-address']);

    // // get property relationship
    // model.forEach((issue) => {
    //   properties.push(issue.get('property'));
    // });

    // // let xhr finish and get the addresses
    // let resolvedProperties = await all(properties);

    // resolvedProperties.forEach((property) => {
    //   addresses.push(property.get('address'))
    // });

    // await all(addresses);

    let t1 = performance.now();

    set(model, 'timeTaken', `fully resolved nested relationships took ${(t1 - t0)} milliseconds.`);

    return model;
  }
});
